# SCSS Mixins #

The SCSS Mixins library contains a suite of mixins that can be utilized in other SCSS projects. 

This suite does not contain any styles itself, only utilities to affect styles. 

## Rules: ##

* The mixins.scss file should only contain mixins (functions) and variables SPECIFIC to a mixin
* The mixins.scss file should NEVER include other scss files
* The mixins.scss file should compile to a css file that only includes: ```/*# sourceMappingURL=mixins.css.map */```


